FROM oracle/graalvm-ce:19.2.1
VOLUME /tmp
COPY build/libs/lambdafinance-0.0.1-SNAPSHOT.jar app.jar
EXPOSE 8080
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]