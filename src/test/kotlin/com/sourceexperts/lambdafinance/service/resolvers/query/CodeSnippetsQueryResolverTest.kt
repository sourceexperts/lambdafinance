package com.sourceexperts.lambdafinance.service.resolvers.query

import com.sourceexperts.lambdafinance.entity.CodeSnippet
import com.sourceexperts.lambdafinance.repository.CodeSnippetRepository
import org.junit.After
import org.junit.Assert
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired

internal class CodeSnippetsQueryResolverTest : AuthorizedTest() {
    @Autowired
    lateinit var codeSnippetsQueryResolver: CodeSnippetsQueryResolver

    @Autowired
    lateinit var codeSnippetRepository: CodeSnippetRepository

    @Test
    fun whenGetAllSnippets_thenShouldOK() {

        codeSnippetRepository.save(CodeSnippet("n1", "dum"))
        codeSnippetRepository.save(CodeSnippet("n2", "dumdidum"))
        val allSnippets = codeSnippetsQueryResolver.getCodeSnippets();
        Assert.assertEquals(2, allSnippets.size)
    }

    @Test
    fun whenGetMySnippets_thenShouldOK() {

        val nonUserSnippet = codeSnippetRepository.save(CodeSnippet("n1", "dum"))
        val userSnippet = codeSnippetRepository.saveAndFlush(CodeSnippet("n2", "dumdidum", user))
        userRepository.save(user.apply { codeSnippets.add(userSnippet) })
        val mySnippets = codeSnippetsQueryResolver.getMyCodeSnippets();
        Assert.assertEquals(1, mySnippets.size)
        Assert.assertEquals(userSnippet, mySnippets[0])
    }

    @Test
    fun whenFindSnippetsByTags_thenShouldOK() {
        codeSnippetRepository.save(CodeSnippet("n1", "dum", mutableListOf("awesome", "good")))
        codeSnippetRepository.save(CodeSnippet("n2", "dum", mutableListOf("awesome", "good")))
        codeSnippetRepository.save(CodeSnippet("n3", "dum", mutableListOf("awesome", "slow")))

        var codeSnippetsByTags = codeSnippetsQueryResolver.getCodeSnippetsByTags(listOf("good"))
        Assert.assertEquals(2, codeSnippetsByTags.size)


        codeSnippetsByTags = codeSnippetsQueryResolver.getCodeSnippetsByTags(listOf("awesome"))
        Assert.assertEquals(3, codeSnippetsByTags.size)

        codeSnippetsByTags = codeSnippetsQueryResolver.getCodeSnippetsByTags(listOf())
        Assert.assertEquals(3, codeSnippetsByTags.size)


    }

    @After
    override fun tearDown() {
        userRepository.deleteAll()
        codeSnippetRepository.deleteAll();
    }
}