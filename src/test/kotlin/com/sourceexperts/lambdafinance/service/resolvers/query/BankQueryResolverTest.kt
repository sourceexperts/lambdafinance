package com.sourceexperts.lambdafinance.service.resolvers.query

import com.sourceexperts.lambdafinance.entity.Bank
import com.sourceexperts.lambdafinance.repository.BankRepository
import org.junit.After
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
internal class BankQueryResolverTest : AuthorizedTest() {
    @Autowired
    lateinit var bankRepository: BankRepository
    @Autowired
    lateinit var bankQueryResolver: BankQueryResolver


    @Test
    fun whenMyBanks_thenReturnOnlyUserBanks() {
        //given
//        val nonUserBank = bankRepository.save(Bank("BBVA"))
//        val userBank = bankRepository.saveAndFlush(Bank("SBERBANK"))
//        userRepository.save(user.apply { banks.add(UserBank(user, userBank)) })
//
//        //when
//        val myBanks = bankQueryResolver.myBanks()
//        Assert.assertEquals(1, myBanks.size)
//        Assert.assertEquals(userBank, myBanks.first())
    }

    @Test
    fun whenAvailableBanks_thenReturnOnlyAvailableBanks() {
//        //given
//        val nonUserBank = bankRepository.save(Bank("BBVA"))
//        val userBank = bankRepository.save(Bank("SBERBANK"))
//        userRepository.save(user.apply { banks.add(userBank) })
//
//        //when
//        val availableBanks = bankQueryResolver.availableBanks()
//        Assert.assertEquals(1, availableBanks.size)
//        Assert.assertEquals(nonUserBank, availableBanks.first())
    }

    @Test(expected = DataIntegrityViolationException::class)
    fun whenSaveDuplicateBank_thenShouldNotPersist() {
        //when
        bankRepository.save(Bank("BBVA"))
        bankRepository.save(Bank("BBVA"))
    }

    @After
    override fun tearDown() {
        userRepository.deleteAll()
        bankRepository.deleteAll()
    }
}