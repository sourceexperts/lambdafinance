package com.sourceexperts.lambdafinance.service.resolvers.mutation

import com.sourceexperts.lambdafinance.entity.EarlySubscriber
import com.sourceexperts.lambdafinance.repository.EarlySubscriberRepository
import org.junit.After
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
internal class EarlySubscriberMutationTest {

    @Autowired
    lateinit var earlySubscriberRepository: EarlySubscriberRepository;

    @Autowired
    lateinit var earlySubscriberMutation: EarlySubscriberMutation


    @Test
    fun whenEmailNotExists_thenShouldPersist() {
        //given
        val email = "sub@aa.ru";
        assertFalse(earlySubscriberRepository.existsByEmail(email))

        //when
        earlySubscriberMutation.subscribe(email)

        //then
        assertTrue(earlySubscriberRepository.existsByEmail(email))
    }

    @Test(expected = IllegalArgumentException::class)
    fun whenEmailAlreadyExists_thenShouldNotPersist() {
        //given
        val email = "sub@aa.ru";
        earlySubscriberRepository.save(EarlySubscriber(email))
        assertTrue(earlySubscriberRepository.existsByEmail(email))

        //when
        earlySubscriberMutation.subscribe(email)
    }

    @After
    fun tearDown() {
        earlySubscriberRepository.deleteAll();
    }
}