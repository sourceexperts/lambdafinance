package com.sourceexperts.lambdafinance.service.resolvers.query

import com.sourceexperts.lambdafinance.entity.User
import com.sourceexperts.lambdafinance.repository.UserRepository
import com.sourceexperts.lambdafinance.service.UserService
import com.sourceexperts.lambdafinance.service.resolvers.mutation.UserMutation
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class AuthorizedTest {


    @Autowired
    lateinit var authenticationProvider: AuthenticationProvider

    @Autowired
    lateinit var userService: UserService;

    @Autowired
    lateinit var userRepository: UserRepository;

    lateinit var user: User
//    @WithUserDetails
//    https://pattern-match.com/blog/2018/10/17/springboot2-with-oauth2-integration/
    @Before
    fun setUp() {
    user = userService.createUser(UserMutation.UserDTO("a", "b", "admin", "admin"))

        val token = UsernamePasswordAuthenticationToken("admin", "admin")
        val authorizedToken = authenticationProvider.authenticate(token)
        SecurityContextHolder.getContext().authentication = authorizedToken;
    }

    @Test
    fun contextLoads() {
        Assert.assertNotNull(userRepository)
        Assert.assertNotNull(user)
    }

    @After
    fun tearDown() {
        userRepository.deleteAll()
    }


}
