package com.sourceexperts.lambdafinance.service.resolvers.mutation

import com.sourceexperts.lambdafinance.currentUser
import com.sourceexperts.lambdafinance.entity.Bank
import com.sourceexperts.lambdafinance.entity.CodeSnippet
import com.sourceexperts.lambdafinance.repository.BankRepository
import com.sourceexperts.lambdafinance.repository.CodeSnippetRepository
import com.sourceexperts.lambdafinance.service.resolvers.query.AuthorizedTest
import org.junit.Assert
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull


internal class CodeSnippetMutationResolverTest : AuthorizedTest() {


    @Autowired
    lateinit var bankRepository: BankRepository

    @Autowired
    lateinit var snippetMutationResolver: CodeSnippetMutationResolver

    @Autowired
    lateinit var codeSnippetRepository: CodeSnippetRepository


    @Test
    fun whenCreateSnippet_thenShouldPersist() {
        //given
        val codeSnippet = CodeSnippet("name", "console.log('hi);")
        //when
        val createdCodeSnippet = snippetMutationResolver.createCodeSnippet(codeSnippet)
        //then
        Assert.assertTrue(codeSnippetRepository.existsById(createdCodeSnippet.id!!))
        assertEquals(codeSnippet.name, createdCodeSnippet.name)
        assertEquals(codeSnippet.code, createdCodeSnippet.code)
        assertEquals(user, createdCodeSnippet.user)
    }

    @Test
    fun whenUpdateSnippet_thenShouldPersist() {
        //given
        //code snippet for current user
        val snippet = snippetMutationResolver.createCodeSnippet(CodeSnippet("name1", "dumdidum"))

        //when
        val updatedSnippet = snippetMutationResolver.updateCodeSnippet(
                snippet.id!!,
                snippet.apply { name = "new name"; code = "new code" })

        //then
        assertEquals("new name", updatedSnippet.name)
        assertEquals("new code", updatedSnippet.code)
        assertEquals(currentUser(), updatedSnippet.user)
        assertNull(updatedSnippet.bank)
    }

    @Test
    fun whenAddSnippetToUser_thenShouldPersist() {
        //given
        val snippet = CodeSnippet("name1", "dumdidum")
        codeSnippetRepository.save(snippet)
        val bank = bankRepository.save(Bank("BBVA"))

        //when
        snippetMutationResolver.connectCodeSnippet(snippet.id!!, bank.id!!)

        //then
        val updatedSnippet = codeSnippetRepository.findByIdOrNull(snippet.id!!)
        assertEquals(currentUser(), updatedSnippet!!.user)
        assertEquals(bank, updatedSnippet.bank)
    }
}