package com.sourceexperts.lambdafinance.service

import com.sourceexperts.lambdafinance.entity.User
import com.sourceexperts.lambdafinance.repository.UserRepository
import com.sourceexperts.lambdafinance.service.resolvers.mutation.UserMutation
import org.junit.After
import org.junit.Assert
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.Bean
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.test.context.junit4.SpringRunner


@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
internal class UserServiceTest {

    @Autowired
    lateinit var userService: UserService

    @Bean
    fun encoder(): PasswordEncoder {
        return BCryptPasswordEncoder(11)
    }

    @Autowired
    lateinit var userRepository: UserRepository

    val adminUser = User("adminName", "adminSurname",
            "admin", encoder().encode("123"))

    @Before
    fun setUp() {
        userRepository.save(adminUser);
    }

    @Test
    fun givenUserExists_whenGetUser_thenShouldReturnUser() {
        //when
        val user = userService.getUser(adminUser.username)
        //then
        Assert.assertEquals(adminUser, user)
    }

    @Test(expected = UsernameNotFoundException::class)
    fun givenUserNotExists_whenGetUser_thenShouldNotReturnUser() {
        userService.getUser("notExistingUsername")
    }

    @Test
    fun whenCreateUser_thenShouldPersist() {
        //given
        assertFalse(userRepository.existsByUsername("c"))
        //when
        userService.createUser(UserMutation.UserDTO("a", "b", "c", "d"))
        assertTrue(userRepository.existsByUsername("c"))
    }

    @Test(expected = DataIntegrityViolationException::class)
    fun whenCreateDuplicateUser_thenShouldThrowException() {
        //when
        userService.createUser(UserMutation.UserDTO("a", "b", "admin", "d"))
    }


    @After
    fun tearDown() {
        userRepository.deleteAll()
    }
}