package com.sourceexperts.lambdafinance

import com.sourceexperts.lambdafinance.config.BanksConfig
import com.sourceexperts.lambdafinance.service.BasicAuthRequestEnhancer
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner


@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class LambdaFinanceApplicationTests {

    @Autowired
    lateinit var banksConfig: BanksConfig

    @Autowired
    lateinit var basicAuthRequestEnhancer: BasicAuthRequestEnhancer

    @Test
    fun contextLoads() {
        assert(basicAuthRequestEnhancer.appId == "app.bbva.thisbank")
        assert(banksConfig.auth[0].name == "bbva")
        assert(banksConfig.auth[1].name == "aabv")

    }


}
