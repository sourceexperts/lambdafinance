package com.sourceexperts.lambdafinance

import com.sourceexperts.lambdafinance.entity.BankAccessToken
import com.sourceexperts.lambdafinance.repository.BankRepository
import com.sourceexperts.lambdafinance.service.BankService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
class BanksAuthController(private val bankService: BankService, private val bankRepository: BankRepository) {

    @GetMapping("/token")
    fun token(@RequestParam(name = "code") code: String, @RequestParam(name = "state") stateKey: String): BankAccessToken {
        return bankService.token(bankRepository.findByName("bbva"), code, stateKey)
    }

    data class Event(
        val subscriptionId: String,
        val resourceId: String,
        val resourceLocation: String,
        val eventType: String
    )

    @PostMapping("/notifications")
    fun receiveNotification(@RequestBody event: Event): String {
        println("event$event")
        return bankService.activateSub(event)
    }
}
