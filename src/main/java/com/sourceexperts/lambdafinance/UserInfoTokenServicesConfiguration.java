package com.sourceexperts.lambdafinance;

import java.util.List;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.autoconfigure.security.oauth2.resource.UserInfoRestTemplateCustomizer;
import org.springframework.boot.autoconfigure.security.oauth2.resource.UserInfoRestTemplateFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;


@Configuration
public class UserInfoTokenServicesConfiguration
{
    @Bean
    @Primary
    public UserInfoRestTemplateFactory userInfoRestTemplateFactory(
        ObjectProvider<List<UserInfoRestTemplateCustomizer>> customizers,
        ObjectProvider<OAuth2ProtectedResourceDetails> details,
        ObjectProvider<OAuth2ClientContext> oauth2ClientContext)
    {

        return new MyDefaultUserInfoRestTemplateFactory(customizers, details,
            oauth2ClientContext);
    }

}
