package com.sourceexperts.lambdafinance.service.banks

import com.google.gson.Gson
import com.sourceexperts.lambdafinance.dto.BbvaContractsResponse
import com.sourceexperts.lambdafinance.dto.Contracts
import com.sourceexperts.lambdafinance.entity.UserBank
import com.sourceexperts.lambdafinance.service.BankService
import khttp.responses.Response
import org.graalvm.polyglot.PolyglotException
import org.springframework.http.HttpHeaders.AUTHORIZATION
import org.springframework.http.HttpHeaders.CONTENT_TYPE
import org.springframework.http.MediaType.APPLICATION_JSON_VALUE
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.ExceptionHandler

@Component
class BbvaApi(var bankService: BankService) {
    val accountUrl = "https://apis.bbva.com/accounts-sbx/v1/me/accounts"
    val transferUrl = "https://apis.bbva.com/payments-sbx/v1/me/transfers"

//    @ExceptionHandler(WebClientResponseException::class)
//    fun handleWebClientResponseException(ex: WebClientResponseException): ResponseEntity<String> {
//        println("Error from WebClient - Status {}, Body {}" + ex.rawStatusCode + ex.responseBodyAsString + ex)
//        return ResponseEntity.status(ex.rawStatusCode).body(ex.responseBodyAsString)
//    }

    @ExceptionHandler(org.graalvm.polyglot.PolyglotException::class)
    fun handlePolyglotException(ex: PolyglotException) {
        println("Error from PolyglotException2 $ex")
    }

    fun getContractsFull(userBank: UserBank): Contracts {
        val url = "https://apis.bbva.com/customers-sbx/v1/me/globalposition-full"
        val token = bankService.getToken(userBank)

        val response: Response = khttp.get(
            url = url,
            headers = mapOf(
                CONTENT_TYPE to APPLICATION_JSON_VALUE,
                AUTHORIZATION to "jwt ${token.accessToken}"
            ))

        val result = Gson().fromJson(response.text, BbvaContractsResponse::class.java)
        println("getResults: $result")
        return result.data
    }


}