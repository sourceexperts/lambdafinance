package com.sourceexperts.lambdafinance.service.banks

import com.google.gson.Gson
import com.sourceexperts.lambdafinance.OtpController
import com.sourceexperts.lambdafinance.currentUser
import com.sourceexperts.lambdafinance.entity.UserBank
import com.sourceexperts.lambdafinance.repository.UserBankRepository
import com.sourceexperts.lambdafinance.service.BankService
import khttp.responses.Response
import org.graalvm.polyglot.PolyglotException
import org.json.JSONObject
import org.springframework.http.HttpHeaders.AUTHORIZATION
import org.springframework.http.HttpHeaders.CONTENT_TYPE
import org.springframework.http.MediaType.APPLICATION_JSON_VALUE
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.ExceptionHandler
import java.net.URL
import java.util.UUID

@Component
class DBApi(var bankService: BankService,
            val otpController: OtpController,
            val userBankRepository: UserBankRepository

) {

    val payRequests = mutableMapOf<String, PayRequest>()

    @ExceptionHandler(org.graalvm.polyglot.PolyglotException::class)
    fun handlePolyglotException(ex: PolyglotException) {
        println("Error from PolyglotException2 $ex")
    }

    fun getCashAccounts(userBank: UserBank): DBAccountsResponse {
        val url = "https://simulator-api.db.com:443/gw/dbapi/banking/cashAccounts/v2"
        val token = userBank.token.accessToken
        val response: Response = khttp.get(
            url = url,
            headers = mapOf(
                CONTENT_TYPE to APPLICATION_JSON_VALUE,
                AUTHORIZATION to "Bearer $token"
            ))

        println("getResults: ${response.text}")
        return Gson().fromJson(response.text, DBAccountsResponse::class.java)
    }

    fun getOTPs(userBank: UserBank): String {
        val url = "https://simulator-api.db.com:443/gw/dbapi/v1/onetimepasswords"
        val response = khttp.get(
            url = url,
            headers = mapOf(
                CONTENT_TYPE to APPLICATION_JSON_VALUE,
                AUTHORIZATION to "Bearer ${userBank.token.accessToken}"
            )
        )
        otpController.sendEvent(response.text)
        return response.text
    }

    fun createOPTChallengeWithId(bankId: Long): String {
        val userBank = userBankRepository.findByUser_IdAndBank_Id(currentUser().id!!, bankId)
        return createOPTChallenge(userBank)

    }

    fun createOPTChallenge(userBank: UserBank): String {
        val url = "https://simulator-api.db.com:443/gw/dbapi/others/onetimepasswords/v2/single"
        val response = khttp.post(
            url = url,
            headers = mapOf(
                CONTENT_TYPE to APPLICATION_JSON_VALUE,
                AUTHORIZATION to "Bearer ${userBank.token.accessToken}"
            ),
            json = JSONObject(CreateOPTChallenge(
                requestData = JSONObject(
                    mapOf<String, String>(
                        "type" to "challengeRequestDataInstantSepaCreditTransfers",
                        "targetIban" to "DE10010000000000005461",
                        "amountValue" to "1",
                        "amountCurrency" to "EUR"
                    )
            )))
        )

        val url1 = URL(response.headers["location"])
        val requestId = url1.path.split("/").last()
        println("for url $url1 request $requestId")

        otpController.sendEvent(requestId)
        return response.text
    }

    data class CreateOPTChallenge(
        val method: String = "MTAN",
        val requestType: String = "INSTANT_SEPA_CREDIT_TRANSFERS",
        val requestData: JSONObject
    )

    data class PayRequest(
        val ibanFrom: String, val ibanTo: String, val amount: Double, val receiverName: String
    )

    fun pay(ibanFrom: String, ibanTo: String, amount: Double, receiverName: String) {
        val userBank = userBankRepository.findByUser_IdAndBank_Id(currentUser().id!!, 1)

        val payRequest = PayRequest(ibanFrom, ibanTo, amount, receiverName)

        createPayChallenge(userBank, payRequest)

    }

    fun createPayChallenge(userBank: UserBank, payRequest: PayRequest): String {
        val url = "https://simulator-api.db.com:443/gw/dbapi/others/onetimepasswords/v2/single"
        val response = khttp.post(
            url = url,
            headers = mapOf(
                CONTENT_TYPE to APPLICATION_JSON_VALUE,
                AUTHORIZATION to "Bearer ${userBank.token.accessToken}"
            ),
            json = JSONObject(CreateOPTChallenge(
                requestData = JSONObject(
                    mapOf<String, Any>(
                        "type" to "challengeRequestDataInstantSepaCreditTransfers",
                        "targetIban" to payRequest.ibanTo,
                        "amountValue" to payRequest.amount,
                        "amountCurrency" to "EUR"
                    )
                )
            ))
        )

        val url1 = URL(response.headers["location"])
        val requestId = url1.path.split("/").last()

        payRequests[requestId] = payRequest
        println("for url $url1 request $requestId")
        otpController.sendEvent(requestId)


        return response.text
    }

    data class OtpResponse(val response: String)

    fun confirmOPTChallenge(userBank: UserBank, responseId: String, tan: String): String {
        var url = "https://simulator-api.db.com:443/gw/dbapi/others/onetimepasswords/v2/single"
        val payRequest = payRequests[responseId] ?: throw IllegalAccessException("No such responseId $responseId in payRequests")
        val response = khttp.patch(
            url = "$url/$responseId",
            headers = mapOf(
                CONTENT_TYPE to APPLICATION_JSON_VALUE,
                AUTHORIZATION to "Bearer ${userBank.token.accessToken}"
            ),
            json = JSONObject(OtpResponse(tan))
        )
        val otp = response.jsonObject["otp"].toString()
        println("otp $otp")


        url = "https://simulator-api.db.com:443/gw/dbapi/paymentInitiation/payments/v1/instantSepaCreditTransfers"
        val resp = khttp.post(
            url = url,
            headers = mapOf(
                CONTENT_TYPE to APPLICATION_JSON_VALUE,
                AUTHORIZATION to "Bearer ${userBank.token.accessToken}",
                "idempotency-id" to UUID.randomUUID().toString(),
                "otp" to otp
            ),
            json = PaymentRequest(
                debtorAccount = PaymentAccount(payRequest.ibanFrom, "EUR"),
                creditorAccount = PaymentAccount(payRequest.ibanTo, "EUR"),
                instructedAmount = Money(payRequest.amount, "EUR"),
                creditorName = payRequest.receiverName
            ).toJson()

        )
        println(resp.text)

        return resp.text

    }

    data class Transaction(val name: String)

    fun postSub(iban: String): String {
        val userBank = userBankRepository.findByUser_IdAndBank_Id(currentUser().id!!, 1)
        val url = "https://simulator-api.db.com:443/gw/dbapi/notifications/subscriptions/v1/transactions"
        val response = khttp.post(
            url = url,
            headers = mapOf(
                CONTENT_TYPE to APPLICATION_JSON_VALUE,
                AUTHORIZATION to "Bearer ${userBank.token.accessToken}",
                "Idempotency-ID" to UUID.randomUUID().toString()
            ),
            json = JSONObject("""
            {
                "filterCriteria": {
                    "iban": "$iban",
                    "incoming": true,
                    "outgoing": true
                    },
                "subscriptionDetails": {
                    "expirationDate": "2019-12-31",
                         "notificationURL": "https://api.lambda.finance/notifications",
                   "subscriptionType": "recurring"
                 }
            }"""))

//        "notificationURL": "https://65d8f844.ngrok.io/notifications",

        //                 "notificationURL": "https://api.lambda.finance/notifications",
        return response.text
    }
}

data class Money(val amount: Double, val currencyCode: String)
data class Address(
    val street: String = "",
    val buildingNumber: String = "",
    val city: String = "",
    val postalCode: String = "",
    val country: String = ""
)

data class PaymentAccount(
    val iban: String,
    val currencyCode: String
)

data class PaymentRequest(
    val debtorAccount: PaymentAccount,
    val creditorAccount: PaymentAccount,
    val creditorName: String,
    val endToEndIdentification: String? = null,
    val instructedAmount: Money,
    val creditorAgent: String? = null,
    val creditorAddress: Address? = null,
    val remittanceInformationUnstructured: String? = null
)

data class DBAccountsResponse(
    val accounts: List<DBCashAccount>
)

data class DBCashAccount(
    val iban: String,
    val currencyCode: String,
    val currentBalance: Double,
    val productDescription: String
)


fun Any.toJson() = JSONObject(this)