package com.sourceexperts.lambdafinance.service.notifications

import com.sourceexperts.lambdafinance.service.BankService
import org.springframework.stereotype.Service

@Service
class NotificationsService(var bankService: BankService) {
//    val url = "https://apis.bbva.com/notifications-sbx/v1/backoffice/configuration"
//
//    fun createConfig(userBank: UserBank): BbvaNotificationResponse {
//        val token = bankService.getToken(userBank)
//
//        val client = WebClient
//            .builder()
//            .baseUrl(url)
//            .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
//            .defaultUriVariables(Collections.singletonMap("url", url))
//            .build()
//
//        val request = client.post()
//            .uri("$url")
//            .header(HttpHeaders.AUTHORIZATION, "jwt ${token.accessToken}")
//            .body(BodyInserters.fromObject(ConfigurationMessage("", "")))
//        val result = request.retrieve()
//            .bodyToMono(BbvaNotificationResponse::class.java)
//            .block()
//        println("configureCallback $result")
//        return result!!
//
//    }
//
//    fun getConfig(userBank: UserBank): BbvaNotificationResponse {
//        val token = bankService.getToken(userBank)
//
//        val client = WebClient
//            .builder()
//            .baseUrl(url)
//            .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
//            .defaultUriVariables(Collections.singletonMap("url", url))
//            .build()
//
//        val request = client.post()
//            .uri("$url")
//            .header(HttpHeaders.AUTHORIZATION, "jwt ${token.accessToken}")
//            .body(BodyInserters.fromObject(ConfigurationMessage("", "")))
//        val result = request.retrieve()
//            .bodyToMono(BbvaNotificationResponse::class.java)
//            .block()
//        println("configureCallback $result")
//        return result!!
//
//    }
//
//    fun updateConfig(userBank: UserBank): BbvaNotificationResponse {
//        val token = bankService.getToken(userBank)
//
//        val client = WebClient
//            .builder()
//            .baseUrl(url)
//            .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
//            .defaultUriVariables(Collections.singletonMap("url", url))
//            .build()
//
//        val request = client.put()
//            .uri("$url")
//            .header(HttpHeaders.AUTHORIZATION, "jwt ${token.accessToken}")
//            .body(BodyInserters.fromObject(ConfigurationMessage("", "")))
//        val result = request.retrieve()
//            .bodyToMono(BbvaNotificationResponse::class.java)
//            .block()
//        println("configureCallback $result")
//        return result!!
//
//    }
//
//    @ExceptionHandler(WebClientResponseException::class)
//    fun handleWebClientResponseException(ex: WebClientResponseException): ResponseEntity<String> {
//        println("Error from WebClient - Status {}, Body {}" + ex.rawStatusCode + ex.responseBodyAsString + ex)
//        return ResponseEntity.status(ex.rawStatusCode).body(ex.responseBodyAsString)
//    }
//
//    @ExceptionHandler(org.graalvm.polyglot.PolyglotException::class)
//    fun handlePolyglotException(ex: PolyglotException) {
//        println("Error from PolyglotException2 $ex")
//    }

}

data class ConfigurationMessage(
    val pubSecret: String,
    val callback: String
)