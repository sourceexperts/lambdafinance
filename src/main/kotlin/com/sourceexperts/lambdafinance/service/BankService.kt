package com.sourceexperts.lambdafinance.service

import com.google.gson.Gson
import com.sourceexperts.lambdafinance.BanksAuthController
import com.sourceexperts.lambdafinance.config.BanksConfig
import com.sourceexperts.lambdafinance.currentUser
import com.sourceexperts.lambdafinance.entity.Bank
import com.sourceexperts.lambdafinance.entity.BankAccessToken
import com.sourceexperts.lambdafinance.entity.User
import com.sourceexperts.lambdafinance.entity.UserBank
import com.sourceexperts.lambdafinance.repository.BankRepository
import com.sourceexperts.lambdafinance.repository.UserBankRepository
import com.sourceexperts.lambdafinance.service.resolvers.mutation.BankMutationResolver
import lombok.extern.slf4j.Slf4j
import org.json.JSONObject
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders.AUTHORIZATION
import org.springframework.http.HttpHeaders.CONTENT_TYPE
import org.springframework.http.MediaType.APPLICATION_JSON_VALUE
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.scheduling.annotation.Schedules
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext
import org.springframework.security.oauth2.client.OAuth2RestTemplate
import org.springframework.security.oauth2.client.resource.UserRedirectRequiredException
import org.springframework.security.oauth2.client.token.DefaultAccessTokenRequest
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeAccessTokenProvider
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeResourceDetails
import org.springframework.stereotype.Service
import org.springframework.util.Base64Utils.encodeToString
import java.time.LocalDateTime

@Slf4j
@Service
class BankService {

    @Autowired
    lateinit var details: AuthorizationCodeResourceDetails

    @Autowired
    lateinit var banksConfig: BanksConfig

    @Autowired
    lateinit var userBankRepository: UserBankRepository

    @Autowired
    lateinit var provider: AuthorizationCodeAccessTokenProvider

    @Autowired
    lateinit var enhancer: BasicAuthRequestEnhancer

    @Autowired
    lateinit var bankRepository: BankRepository


    var defaultOAuth2ClientContext = DefaultOAuth2ClientContext()
    val contextMap = mutableMapOf<String, User>()

    val tokenUrl: String = "https://localhost:8080/token"

    /**
     * @return redirect url
     */
    fun authorize(bank: Bank): BankMutationResolver.AuthRedirect {
        val atr = DefaultAccessTokenRequest()
        defaultOAuth2ClientContext = DefaultOAuth2ClientContext(atr)
        //todo use bank object to instantiate rest template
        val bankRestTemplate = OAuth2RestTemplate(details, defaultOAuth2ClientContext)
        try {
            val accessToken = bankRestTemplate.accessToken
            accessToken.value
            return BankMutationResolver.AuthRedirect("null", "null")//TODO
        } catch (e: UserRedirectRequiredException) {
            var redirectUrl = e.redirectUri.replace("http:", "https:") + "?"
            e.requestParams.forEach { (name, value) -> redirectUrl += "$name=$value&" }
            redirectUrl += "redirect_uri=$tokenUrl&state=${e.stateKey}"
            contextMap[e.stateKey] = currentUser()
            return BankMutationResolver.AuthRedirect(redirectUrl, e.stateKey)
        }
    }

    fun token(bank: Bank, code: String, stateKey: String): BankAccessToken {

        val response = khttp.post(url = "https://simulator-api.db.com/gw/oidc/token",
            params = mapOf(
                "code" to code,
                "grant_type" to "authorization_code",
                "state" to stateKey,
                "redirect_uri" to "$tokenUrl"

            ),
            headers = mapOf(
                AUTHORIZATION to "Basic " + encodeToString(
                    """${banksConfig.auth[0].appId}:${banksConfig.auth[0].secretId}""".toByteArray()),
                CONTENT_TYPE to APPLICATION_JSON_VALUE
            )
        )
        val accessTokenDTO = Gson().fromJson(response.text, BankAccessTokenDTO::class.java)


        val token = BankAccessToken(accessTokenDTO!!)

        val user = contextMap[stateKey]!!
        val userBank = userBankRepository.findByUserAndBank(user, bank)
                ?: UserBank(user, bank, token)
        userBank.token = token
        userBankRepository.save(userBank)
        return token
    }

    fun refreshToken(userBank: UserBank): BankAccessToken {

        val response = khttp.post(url = "https://simulator-api.db.com/gw/oidc/token",
            params = mapOf(
                "grant_type" to "refresh_token",
                "redirect_uri" to tokenUrl,
                "client_id" to banksConfig.auth[0].appId,
                "secret_id" to banksConfig.auth[0].secretId
            ),
            headers = mapOf(
                AUTHORIZATION to "Basic " + encodeToString(
                    """${banksConfig.auth[0].appId}:${banksConfig.auth[0].secretId}""".toByteArray()),
                CONTENT_TYPE to APPLICATION_JSON_VALUE
            ),
            data = mapOf("refresh_token" to userBank.token.refreshToken)
        )
        val refreshedTokenDTO = Gson().fromJson(response.text, BankAccessTokenDTO::class.java)

        val refreshedToken = BankAccessToken(refreshedTokenDTO)
        userBank.token = refreshedToken
        userBankRepository.save(userBank)

        println("bankRefreshToken $refreshedToken")
        return refreshedToken
    }

    data class BankAccessTokenDTO(
            var access_token: String,
            var refresh_token: String?="",
            var scope: String?="",
            var expires_in: Long,
            var refresh_expires_in: Long?=0
    )


    @Schedules(Scheduled(fixedRate = 5 * 60 * 1000))//every 5 minutes
    fun renewTokens() {
        println("renewTokens started")
        userBankRepository.findAll()
                .filter { it.token.refreshExpiresAt.isAfter(LocalDateTime.now().plusDays(1)) }
                .onEach {
                    println("refresh token for userbank ${it.id} is expiring in less than one day, will refresh")
                }
                .map(this::refreshToken)
        println("renewTokens done")

    }

    fun getToken(userBank: UserBank): BankAccessToken {
        if (userBank.token.expiresAt.isBefore(LocalDateTime.now())) {
            return refreshToken(userBank)
        }
        return userBank.token
    }

    fun activateSub(event: BanksAuthController.Event): String {

        val userBank = userBankRepository.getOne(47)

        val url = "https://simulator-api.db.com:443/gw/dbapi/notifications/subscriptions/v1"
        val response = khttp.patch(
            url = "$url/${event.subscriptionId}",
            headers = mapOf(
                CONTENT_TYPE to APPLICATION_JSON_VALUE,
                AUTHORIZATION to "Bearer ${userBank.token.accessToken}"),
            json = JSONObject(mapOf("activationCode" to event.resourceId)))
        println(response.text)
        return response.text
    }
}
