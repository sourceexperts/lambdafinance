package com.sourceexperts.lambdafinance.service.resolvers.mutation

import com.coxautodev.graphql.tools.GraphQLMutationResolver
import com.sourceexperts.lambdafinance.BankEnum.BBVA
import com.sourceexperts.lambdafinance.currentUser
import com.sourceexperts.lambdafinance.entity.BankAccessToken
import com.sourceexperts.lambdafinance.repository.BankRepository
import com.sourceexperts.lambdafinance.repository.UserBankRepository
import com.sourceexperts.lambdafinance.service.BankService
import com.sourceexperts.lambdafinance.service.banks.BbvaApi
import com.sourceexperts.lambdafinance.service.banks.DBApi
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Component
import javax.persistence.EntityNotFoundException

@Component
class BankMutationResolver(
        val userBankRepository: UserBankRepository,
        val bankRepository: BankRepository,
        val bankService: BankService,
        val bbvaApi: BbvaApi,
        val dbApi: DBApi
) : GraphQLMutationResolver {


    fun authorize(bankName: String): AuthRedirect {
        val bank = bankRepository.findByName(BBVA.bankName)
        return bankService.authorize(bank)
    }

    @PreAuthorize("""hasRole("USER")""")
    fun token(authCode: String, stateKey: String): String {
        return bankService.token(bankRepository.findByName("bbva"), authCode, stateKey).accessToken
    }

    @PreAuthorize("""hasRole("USER")""")
    fun refreshToken(): BankAccessToken {
        val userBank = userBankRepository.findByUserAndBank(currentUser(), bankRepository.findByName("bbva"))
                ?: throw EntityNotFoundException("User ${currentUser().id} not connected to bank bbva")
        return bankService.refreshToken(userBank)
    }

    @PreAuthorize("""hasRole("USER")""")
    fun createOtp(bankId: Long): String {
        val userBank = userBankRepository.findByUser_IdAndBank_Id(currentUser().id!!, bankId)
        return dbApi.createOPTChallenge(userBank)
    }

    @PreAuthorize("""hasRole("USER")""")
    fun confirmOtp(bankId: Long, responseId: String, tan: String): String {
        val userBank = userBankRepository.findByUser_IdAndBank_Id(currentUser().id!!, bankId)
        return dbApi.confirmOPTChallenge(userBank, responseId, tan)
    }

    @PreAuthorize("""hasRole("USER")""")
    fun subscribeToUpdates(iban: String): String {
        return dbApi.postSub(iban)
    }




    data class AuthRedirect(val redirectUrl: String, val stateKey: String)
}