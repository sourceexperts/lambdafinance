package com.sourceexperts.lambdafinance.service.resolvers.query

import com.coxautodev.graphql.tools.GraphQLQueryResolver
import com.sourceexperts.lambdafinance.currentUser
import com.sourceexperts.lambdafinance.entity.Bank
import com.sourceexperts.lambdafinance.repository.BankRepository
import com.sourceexperts.lambdafinance.repository.UserBankRepository
import com.sourceexperts.lambdafinance.repository.UserRepository
import com.sourceexperts.lambdafinance.service.banks.DBAccountsResponse
import com.sourceexperts.lambdafinance.service.banks.DBApi
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Service
import javax.persistence.EntityNotFoundException


@Service
class BankQueryResolver(
    val userRepository: UserRepository,
    val bankRepository: BankRepository,
    val userBankRepository: UserBankRepository,
    val dbApi: DBApi
) : GraphQLQueryResolver {

    @PreAuthorize("""hasRole("USER")""")
    fun getBank(name: String): Bank? {
        return bankRepository.findByName(name)
                ?: throw EntityNotFoundException("Bank $name not found")
    }

    @PreAuthorize("""hasRole("USER")""")
    fun myBanks() = userBankRepository.findAllByUser(currentUser())

    @PreAuthorize("""hasRole("USER")""")
    fun availableBanks() = bankRepository.findAll().minus(myBanks())

    @PreAuthorize("""hasRole("USER")""")
    fun accounts(bankId: Long): DBAccountsResponse {
        val userBank = userBankRepository.findByUser_IdAndBank_Id(currentUser().id!!, bankId)
        return dbApi.getCashAccounts(userBank)
    }

    @PreAuthorize("""hasRole("USER")""")
    fun otps(bankId: Long): String {
        val userBank = userBankRepository.findByUser_IdAndBank_Id(currentUser().id!!, bankId)
        return dbApi.getOTPs(userBank)
    }


}