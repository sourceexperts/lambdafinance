package com.sourceexperts.lambdafinance.service.resolvers.mutation

import com.coxautodev.graphql.tools.GraphQLMutationResolver
import com.sourceexperts.lambdafinance.currentUser
import com.sourceexperts.lambdafinance.entity.User
import com.sourceexperts.lambdafinance.repository.EarlySubscriberRepository
import com.sourceexperts.lambdafinance.service.UserService
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Component
import org.springframework.web.context.request.RequestAttributes
import org.springframework.web.context.request.RequestContextHolder
import org.springframework.security.web.context.HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY
import javax.servlet.http.HttpServletRequest

@Component
class UserMutation(val userService: UserService,
                   val encoder: PasswordEncoder,
                   val authenticationProvider: AuthenticationProvider,
                   val earlySubscriberRepository: EarlySubscriberRepository
) : GraphQLMutationResolver {

    data class UserDTO(val firstName: String, val lastname: String, val username: String, val password: String)

    fun createUser(user: UserDTO): User {
        return userService.createUser(user)
    }

    fun signinUser(username: String, password: String): String {
        val user = userService.getUser(username)
        if (encoder.matches(password, user.password)) {
            val token = UsernamePasswordAuthenticationToken(username, password)
            val authorizedToken = authenticationProvider.authenticate(token)

            SecurityContextHolder.getContext().authentication = authorizedToken;
            return "OK"
        }
        throw IllegalArgumentException("Invalid credentials")
    }

    @PreAuthorize("""hasRole("USER")""")
    fun me() = currentUser()
}