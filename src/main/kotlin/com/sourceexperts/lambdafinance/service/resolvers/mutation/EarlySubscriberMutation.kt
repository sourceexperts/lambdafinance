package com.sourceexperts.lambdafinance.service.resolvers.mutation

import com.coxautodev.graphql.tools.GraphQLMutationResolver
import com.sourceexperts.lambdafinance.entity.EarlySubscriber
import com.sourceexperts.lambdafinance.repository.EarlySubscriberRepository
import org.springframework.stereotype.Component


@Component
class EarlySubscriberMutation(val earlySubscriberRepository: EarlySubscriberRepository
) : GraphQLMutationResolver {

    fun subscribe(email: String): String {
        if (earlySubscriberRepository.existsByEmail(email)) {
            throw IllegalArgumentException("Email $email already subscribed")
        }
        earlySubscriberRepository.save(EarlySubscriber(email))
        return "$email subscribed successfully"
    }
}