package com.sourceexperts.lambdafinance.service.resolvers.query

import com.coxautodev.graphql.tools.GraphQLQueryResolver
import com.sourceexperts.lambdafinance.currentUser
import com.sourceexperts.lambdafinance.entity.CodeSnippet
import com.sourceexperts.lambdafinance.repository.CodeSnippetRepository
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Service
import javax.persistence.EntityNotFoundException


@Service
class CodeSnippetsQueryResolver(val codeSnippetRepository: CodeSnippetRepository) : GraphQLQueryResolver {

    @PreAuthorize("""hasRole("USER")""")
    fun getCodeSnippet(name: String): CodeSnippet? {
        return codeSnippetRepository.findByName(name)
                ?: throw EntityNotFoundException("CodeSnippet $name not" +
                        " found")
    }

    @PreAuthorize("""hasRole("USER")""")
    fun getMyCodeSnippets(): List<CodeSnippet> {
        return codeSnippetRepository.findAllWhereUserIs(currentUser())
    }

    @PreAuthorize("""hasRole("USER")""")
    fun getCodeSnippets(): List<CodeSnippet> {
        return codeSnippetRepository.findAll()
    }
}