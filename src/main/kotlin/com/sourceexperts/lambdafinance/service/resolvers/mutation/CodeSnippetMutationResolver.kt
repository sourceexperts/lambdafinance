package com.sourceexperts.lambdafinance.service.resolvers.mutation

import com.coxautodev.graphql.tools.GraphQLMutationResolver
import com.sourceexperts.lambdafinance.currentUser
import com.sourceexperts.lambdafinance.entity.CodeSnippet
import com.sourceexperts.lambdafinance.repository.CodeSnippetRepository
import com.sourceexperts.lambdafinance.repository.UserBankRepository
import com.sourceexperts.lambdafinance.service.runner.CodeRunner
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Component


@Component
class CodeSnippetMutationResolver(
        val userBankRepository: UserBankRepository,
        val codeRunner: CodeRunner,
        val codeSnippetRepository: CodeSnippetRepository
) : GraphQLMutationResolver {

    data class CodeSnippetDTO(val name: String,
                              val code: String)

    @PreAuthorize("""hasRole("USER")""")
    fun createCodeSnippet(bankId: Long, codeSnippetDto: CodeSnippetDTO): CodeSnippet {
        val userBank = userBankRepository.findByUser_IdAndBank_Id(currentUser().id!!, bankId)
        val codeSnippet = CodeSnippet(codeSnippetDto.name, codeSnippetDto.code, userBank)
        return codeSnippetRepository.save(codeSnippet)
    }



    @PreAuthorize("""hasRole("USER")""")
    fun updateCodeSnippet(id: Long, codeSnippetDto: CodeSnippetDTO): CodeSnippet {
        val codeSnippet = codeSnippetRepository.getOne(id)
        codeSnippet.name = codeSnippetDto.name
        codeSnippet.code = codeSnippetDto.code
        codeSnippetRepository.save(codeSnippet)
        return codeSnippet
    }

    @PreAuthorize("""hasRole("USER")""")
    fun executeCodeSnippet(id: Long): String {
        val snippet = codeSnippetRepository.getOne(id);
        codeRunner.execute(snippet)
        return "DONE"
    }

    @PreAuthorize("""hasRole("USER")""")
    fun deleteCodeSnippet(id: Long): String {
        codeSnippetRepository.deleteById(id)
        return "OK"
    }
}