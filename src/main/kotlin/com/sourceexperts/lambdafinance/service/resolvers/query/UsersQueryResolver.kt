package com.sourceexperts.lambdafinance.service.resolvers.query

import com.coxautodev.graphql.tools.GraphQLQueryResolver
import com.sourceexperts.lambdafinance.entity.User
import com.sourceexperts.lambdafinance.service.UserService
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Service


@Service
class UsersQueryResolver(val userService: UserService) : GraphQLQueryResolver {

    @PreAuthorize("""hasRole("USER")""")
    fun getUser(username: String): User? {
        return userService.getUser(username)
    }

    @PreAuthorize("""hasRole("ADMIN")""")
    fun getUsers(): List<User> {
        return userService.getUsers()
    }


}