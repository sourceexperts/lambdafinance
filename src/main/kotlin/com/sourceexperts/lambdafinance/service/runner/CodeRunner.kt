package com.sourceexperts.lambdafinance.service.runner

import com.sourceexperts.lambdafinance.entity.CodeSnippet
import com.sourceexperts.lambdafinance.repository.CodeSnippetRepository
import com.sourceexperts.lambdafinance.service.banks.BbvaApi
import com.sourceexperts.lambdafinance.service.banks.DBApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import org.graalvm.polyglot.Context
import org.graalvm.polyglot.PolyglotException
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.scheduling.annotation.Schedules
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.ExceptionHandler
import java.time.LocalDateTime
import kotlin.random.Random
import kotlin.random.Random.Default.nextInt

@Service
class CodeRunner(var codeSnippetRepository: CodeSnippetRepository,
                 var bbvaApi: BbvaApi,
                 var dbApi: DBApi
                 ) {
    fun execute(codeSnippet: CodeSnippet) {
        println("---------------------------------")
        println("\tRun codeSnippet ${codeSnippet.name} at ${LocalDateTime.now()}")
        println("\tCode ${codeSnippet.code} ")
        val transactions = listOf(Transaction(
            nextInt(-600, -500).div(100.0)
        ), Transaction(
            nextInt(3100_00, 3900_00).div(100.0)
        ))
        //use engine if you want to keep context of task
        Context.newBuilder().allowAllAccess(true)
                .build().use { context ->
                    context.getBindings("js").putMember("bbvaApi", bbvaApi)
                    context.getBindings("js").putMember("dbApi", dbApi)
                    context.getBindings("js").putMember("currentTransaction", transactions.shuffled()[0])
                    context.getBindings("js").putMember("userBank", codeSnippet.userBank)
                    try {
                        context.eval("js", codeSnippet.code)
                    } catch (e: PolyglotException) {
                        handlePolyglotException(e)
                    }
                }
        println("---------------------------------")

    }

    data class Transaction(val amount: Double = 10.0)

//    @Schedules(Scheduled(fixedRate = 30_000))
    fun scheduledRunner() {
        val tasksToRun = codeSnippetRepository.findAllByRepeatableTrue()
//        println("Running ${tasksToRun.size} code snippets that are scheduled to run")

        val deferred = tasksToRun.map { task ->
            GlobalScope.async {
                execute(task)
                1
            }
        }
        runBlocking {

            val success = deferred.sumBy { it.await() }
//            println("$success/${tasksToRun.size} tasks completed")
        }


    }

    @ExceptionHandler(org.graalvm.polyglot.PolyglotException::class)
    fun handlePolyglotException(ex: PolyglotException) {

        println("Error from PolyglotException $ex")
    }
}