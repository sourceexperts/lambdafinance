package com.sourceexperts.lambdafinance.service

import org.springframework.http.HttpHeaders
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails
import org.springframework.security.oauth2.client.token.AccessTokenRequest
import org.springframework.security.oauth2.client.token.DefaultRequestEnhancer
import org.springframework.stereotype.Component
import org.springframework.util.MultiValueMap

class BasicAuthRequestEnhancer(val appId: String, val secretID: String) : DefaultRequestEnhancer() {

    override fun enhance(
            request: AccessTokenRequest,
            resource: OAuth2ProtectedResourceDetails?,
            form: MultiValueMap<String, String>, headers: HttpHeaders) {
        headers.setBasicAuth(appId, secretID)
    }
}