package com.sourceexperts.lambdafinance.service

import com.sourceexperts.lambdafinance.dto.UserPrincipal
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Service

@Service
class UserDetailsService(
        val userService: UserService
) : UserDetailsService {

    override fun loadUserByUsername(username: String) =
            UserPrincipal(userService.getUser(username))

}