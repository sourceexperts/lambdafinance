package com.sourceexperts.lambdafinance.service

import com.sourceexperts.lambdafinance.entity.User
import com.sourceexperts.lambdafinance.repository.UserRepository
import com.sourceexperts.lambdafinance.service.resolvers.mutation.UserMutation
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service

@Service
class UserService(private val userRepository: UserRepository,
                  private val encoder: PasswordEncoder
) {
    fun getUser(username: String): User {
        return userRepository.findByUsername(username) ?: throw UsernameNotFoundException("User $username not" +
                " found");
    }

    fun getUsers(): MutableList<User> = userRepository.findAll()

    fun createUser(userDto: UserMutation.UserDTO): User {

        val user = User(
                firstName = userDto.firstName,
                lastName = userDto.lastname,
                username = userDto.username,
                password = encoder.encode(userDto.password)
        )
//        validate(user)//todo
        return userRepository.saveAndFlush(user)
    }

    private fun validate(user: User) {
        if (userRepository.existsByUsername(user.username)) {
            throw IllegalArgumentException("User ${user.username} already exists!")
        }
    }
}