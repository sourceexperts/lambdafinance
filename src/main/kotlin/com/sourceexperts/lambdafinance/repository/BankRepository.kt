package com.sourceexperts.lambdafinance.repository

import com.sourceexperts.lambdafinance.entity.Bank
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface BankRepository : JpaRepository<Bank, Long> {
    fun findByName(name: String): Bank
}