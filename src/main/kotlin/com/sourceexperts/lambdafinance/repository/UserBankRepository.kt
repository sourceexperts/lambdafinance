package com.sourceexperts.lambdafinance.repository

import com.sourceexperts.lambdafinance.entity.Bank
import com.sourceexperts.lambdafinance.entity.User
import com.sourceexperts.lambdafinance.entity.UserBank
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface UserBankRepository : JpaRepository<UserBank, Long> {
    fun findByUserAndBank(user: User, bank: Bank): UserBank?
    fun findAllByUser(user: User): List<UserBank>
    fun findByUser_IdAndBank_Id(userId: Long, bankId: Long): UserBank
}