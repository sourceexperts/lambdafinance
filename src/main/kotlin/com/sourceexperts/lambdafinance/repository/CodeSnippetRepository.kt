package com.sourceexperts.lambdafinance.repository

import com.sourceexperts.lambdafinance.entity.CodeSnippet
import com.sourceexperts.lambdafinance.entity.User
import com.sourceexperts.lambdafinance.entity.UserBank
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
interface CodeSnippetRepository : JpaRepository<CodeSnippet, Long> {
    fun findByName(name: String): CodeSnippet?
    fun findAllByUserBank(userBank: UserBank): CodeSnippet
    fun findAllByRepeatableTrue(): List<CodeSnippet>
    @Query("SELECT c FROM CodeSnippet c WHERE c.userBank.user = :user")
    fun findAllWhereUserIs(@Param("user") currentUser: User): List<CodeSnippet>;
}