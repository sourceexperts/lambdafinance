package com.sourceexperts.lambdafinance.repository

import com.sourceexperts.lambdafinance.entity.EarlySubscriber
import com.sourceexperts.lambdafinance.entity.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface EarlySubscriberRepository : JpaRepository<EarlySubscriber, Long> {
    fun findByEmail(email: String): EarlySubscriber?
    fun existsByEmail(email: String): Boolean

}