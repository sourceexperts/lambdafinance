package com.sourceexperts.lambdafinance.dto

data class BbvaResponse<T>(var result: BbvaBasicResult, var data: T)
data class BbvaAccountResponse(var result: BbvaBasicResult, var data: AccountData)
data class BbvaAccountsResponse(var result: BbvaBasicResult, var data: AccountsData)

data class BbvaBasicResult(var code: String, var info: String)
data class AccountData(var account: Account)
data class AccountsData(var accounts: List<Account>)

data class Account(
        var id: String?,
        var alias: String?,
        var type: String?,
        var operable: Boolean?,
        var description: String?,
        var number: String?,
        var currency: String?,
        var balance: Double?
        //links field
)

data class BbvaNotificationResponse(var result: BbvaBasicResult)

data class BbvaContractsResponse(var result: BbvaBasicResult, var data: Contracts)
data class Contracts(val contracts: List<Contract>)
data class Contract(
    val id: String,
    val description: String,
    val details: ContractDetails
)

data class ContractDetails(val balance: List<Balance>)
data class Balance(val type: String, val amount: Double, val currency: String)