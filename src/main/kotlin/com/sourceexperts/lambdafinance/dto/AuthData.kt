package com.sourceexperts.lambdafinance.dto

data class AuthData(val username: String, val password: String)