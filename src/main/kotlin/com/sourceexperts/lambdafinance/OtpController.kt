package com.sourceexperts.lambdafinance

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter
import java.util.concurrent.Executors

@Controller
class OtpController {

    private val executor = Executors.newCachedThreadPool()
    var emitter = SseEmitter()

    @GetMapping("/otp")
    fun streamSseMvc(): SseEmitter {
        emitter.onCompletion { emitter = SseEmitter() }
        return emitter
    }

    fun sendEvent(message: String) {
        val event = SseEmitter.event()
            .data(message)
            .name("sse event - mvc")
        emitter.send(event)
    }

}