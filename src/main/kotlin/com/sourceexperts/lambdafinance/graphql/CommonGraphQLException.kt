package com.sourceexperts.lambdafinance.graphql

import graphql.Assert.assertNotNull
import graphql.ErrorType
import graphql.GraphQLError
import graphql.GraphqlErrorHelper
import graphql.execution.ExecutionPath
import graphql.language.SourceLocation
import java.util.*

class CommonGraphQLException(path: ExecutionPath, exception: Throwable, sourceLocations: List<SourceLocation>) :
        GraphQLError {

    private val message: String
    private val path: List<Any> = assertNotNull<ExecutionPath>(path).toList()
    private val exception: Throwable = assertNotNull<Throwable>(exception)
    private val cause: String = assertNotNull<Throwable>(exception).javaClass.canonicalName
    private val locations: List<SourceLocation> = sourceLocations
    private val extensions: Map<String, Any>?

    init {
        this.extensions = mkExtensions(exception)
        this.message = exception.message!!
    }


    /*
     * This allows a DataFetcher to throw a graphql error and have "extension data" be transferred from that
     * exception into the CommonGraphQLException error and hence have custom "extension attributes"
     * per error message.
     */
    private fun mkExtensions(exception: Throwable): Map<String, Any>? {
        var extensions: MutableMap<String, Any>? = null
        if (exception is GraphQLError) {
            val map = (exception as GraphQLError).extensions
            if (map != null) {
                extensions = LinkedHashMap()
                extensions.putAll(map)
            }
        }
        return extensions
    }


    override fun getMessage(): String {
        return message
    }

    override fun getLocations(): List<SourceLocation> {
        return locations
    }

    fun getException(): Throwable {
        return exception
    }

    fun getCause(): String {
        return cause
    }
    override fun getPath(): List<Any> {
        return path
    }

    override fun getExtensions(): Map<String, Any>? {
        return extensions
    }

    override fun getErrorType(): ErrorType {
        return ErrorType.DataFetchingException
    }

    override fun toString(): String {
        return "CommonGraphQLException{" +
                "path=" + path +
                "exception=" + exception +
                "locations=" + locations +
                '}'.toString()
    }

    override fun equals(o: Any?): Boolean {
        return GraphqlErrorHelper.equals(this, o)
    }

    override fun hashCode(): Int {
        return GraphqlErrorHelper.hashCode(this)
    }
}