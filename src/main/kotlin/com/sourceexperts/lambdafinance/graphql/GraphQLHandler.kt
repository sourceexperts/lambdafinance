package com.sourceexperts.lambdafinance.graphql

import graphql.ExceptionWhileDataFetching
import graphql.GraphQLError
import graphql.execution.ExecutionPath
import org.springframework.stereotype.Component


@Component
class MyGraphQLErrorHandler : graphql.servlet.GraphQLErrorHandler {

    override fun processErrors(list: List<GraphQLError>): List<GraphQLError> {
        return list.map { this.getNested(it) }
    }


    private fun getNested(error: GraphQLError): GraphQLError {
        if (error is ExceptionWhileDataFetching) {
            return CommonGraphQLException(ExecutionPath.fromList(error.path), error.exception, error.locations)
        }
        return error
    }
}