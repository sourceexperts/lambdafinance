package com.sourceexperts.lambdafinance.config

import org.springframework.data.rest.core.config.RepositoryRestConfiguration
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter
import org.springframework.stereotype.Component
import org.springframework.web.servlet.config.annotation.CorsRegistry

@Component
class SpringDataRestCustomization : RepositoryRestConfigurerAdapter() {

    override fun configureRepositoryRestConfiguration(config: RepositoryRestConfiguration) {
            config.corsRegistry.addMapping("/**")
                .allowedOrigins("https://lambda.finance", "http://localhost:3000")
                .allowedMethods("POST", "GET", "OPTIONS", "PUT")
                .allowCredentials(true)
                .maxAge(3600)
        }
}