package com.sourceexperts.lambdafinance.config

import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.CorsRegistry
import org.springframework.web.servlet.config.annotation.EnableWebMvc
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

@Configuration
@EnableWebMvc
class WebConfig : WebMvcConfigurer {

    override fun addCorsMappings(registry: CorsRegistry) {
        registry.addMapping("/**")
            .allowedOrigins("https://lambda.finance", "http://localhost:3000")
            .allowedMethods("POST", "GET", "OPTIONS", "PUT")
            .allowCredentials(true)
            .maxAge(3600)
    }

}