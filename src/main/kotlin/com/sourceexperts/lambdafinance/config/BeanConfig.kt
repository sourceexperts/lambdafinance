package com.sourceexperts.lambdafinance.config

import com.sourceexperts.lambdafinance.service.BasicAuthRequestEnhancer
import io.sentry.spring.SentryExceptionResolver
import io.sentry.spring.SentryServletContextInitializer
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.web.servlet.ServletContextInitializer
import org.springframework.context.annotation.Bean
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeAccessTokenProvider
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeResourceDetails
import org.springframework.security.oauth2.provider.token.TokenStore
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore
import org.springframework.stereotype.Component
import org.springframework.web.servlet.HandlerExceptionResolver
import javax.sql.DataSource


@Component
class BeanConfig(var banksConfig: BanksConfig) {
    @Bean
    fun sentryExceptionResolver(): HandlerExceptionResolver {
        return SentryExceptionResolver()
    }

    @Bean
    fun sentryServletContextInitializer(): ServletContextInitializer {
        return SentryServletContextInitializer()
    }

    @Bean
    fun basicAuthRequestEnhancer() = BasicAuthRequestEnhancer(
            banksConfig.auth[0].appId,
            banksConfig.auth[0].secretId
    )

    @Bean
    fun encoder(): PasswordEncoder {
        return BCryptPasswordEncoder(11)
    }

    @Bean
    fun authorizationCodeAccessTokenProvider():
            AuthorizationCodeAccessTokenProvider {
        val provider = AuthorizationCodeAccessTokenProvider()
        provider.setAuthorizationRequestEnhancer(basicAuthRequestEnhancer())//maybe not need
        provider.setTokenRequestEnhancer(basicAuthRequestEnhancer())
        return provider;
    }


    @Bean
    @ConfigurationProperties("test.oauth2")
    fun bank(): AuthorizationCodeResourceDetails {
        return AuthorizationCodeResourceDetails()
    }


    @Bean
    fun tokenStore(dataSource: DataSource): TokenStore {
        return JdbcTokenStore(dataSource)
    }
}

