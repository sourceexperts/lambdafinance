package com.sourceexperts.lambdafinance.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration

@Configuration
@ConfigurationProperties(prefix = "banks")
class BanksConfig {

    lateinit var auth: List<AuthProperties>

    class AuthProperties {
        lateinit var name: String//bank name
        lateinit var appId: String
        lateinit var secretId: String
    }
}