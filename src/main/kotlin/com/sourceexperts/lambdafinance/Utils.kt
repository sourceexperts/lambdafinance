package com.sourceexperts.lambdafinance

import com.sourceexperts.lambdafinance.dto.UserPrincipal
import com.sourceexperts.lambdafinance.entity.User
import org.springframework.core.ParameterizedTypeReference
import org.springframework.security.core.context.SecurityContextHolder

fun currentUser(): User {
    val userPrincipal = SecurityContextHolder.getContext().authentication.principal as UserPrincipal
    return userPrincipal.user
}

fun getAuthentication() = SecurityContextHolder.getContext().authentication

fun currentPrincipal() = SecurityContextHolder.getContext().authentication.principal as UserPrincipal

inline fun <reified T> typeReference() = object : ParameterizedTypeReference<T>() {}

enum class BankEnum(var bankName: String) {
    BBVA("bbva")
}
