package com.sourceexperts.lambdafinance.entity

import java.time.LocalDateTime
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table
import javax.validation.constraints.Email
import javax.validation.constraints.NotNull

@Entity
@Table(name = "early_subscribers")
data class EarlySubscriber(@Email @Id val email: String) {
    @NotNull
    val dateCreated = LocalDateTime.now();
}