package com.sourceexperts.lambdafinance.entity

import javax.persistence.*

@Entity
@Table(name = "banks")
data class Bank(
        @Column(nullable = false, unique = true)
        val name: String
) {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Long? = null
}

