package com.sourceexperts.lambdafinance.entity

import javax.persistence.*
import javax.validation.constraints.Size

@Entity
@Table(name = "users")
data class User(
        @Size(min = 3, max = 20)
        var firstName: String,

        var lastName: String,

        @Size(min = 3, max = 25)
        @Column(nullable = false, unique = true)
        val username: String,

        @Column(nullable = false)
        val password: String
) {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Long? = null

    override fun toString(): String {
        return "User(firstName='$firstName', lastName='$lastName', username='$username', password='$password', id=$id)"
    }
}

