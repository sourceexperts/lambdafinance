package com.sourceexperts.lambdafinance.entity

import com.sourceexperts.lambdafinance.service.BankService
import java.time.LocalDateTime
import javax.persistence.*

@Entity
@Table(name = "user_banks")
data class UserBank(
        @ManyToOne
        @JoinColumn(name = "id_user", nullable = false)
        var user: User,

        @ManyToOne
        @JoinColumn(name = "id_bank", nullable = false)
        var bank: Bank,

        @Embedded
        var token: BankAccessToken

) {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Long? = null
}

@Embeddable
data class BankAccessToken(
        @Column(columnDefinition = "TEXT")
        var accessToken: String,
        @Column(columnDefinition = "TEXT")
        var refreshToken: String,
        @Column(columnDefinition = "TEXT")
        var scope: String,
        var expiresAt: LocalDateTime,
        var refreshExpiresAt: LocalDateTime
) {
    var dateUpdated: LocalDateTime = LocalDateTime.now()

    constructor(bankAccessTokenDTO: BankService.BankAccessTokenDTO) : this(
            accessToken = bankAccessTokenDTO.access_token,
            refreshToken = bankAccessTokenDTO.refresh_token?:"",
            scope = bankAccessTokenDTO.scope?:"",
            expiresAt = LocalDateTime.now().plusSeconds(bankAccessTokenDTO.expires_in),
            refreshExpiresAt = LocalDateTime.now().plusSeconds(bankAccessTokenDTO.refresh_expires_in?:0)
    ) {
        dateUpdated = LocalDateTime.now()
    }
}
