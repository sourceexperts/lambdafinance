package com.sourceexperts.lambdafinance.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
@Table(name = "code_snippets")
data class CodeSnippet(
    var name: String,
    var code: String,

    @ManyToOne
    @JoinColumn(name = "user_bank_id")
    val userBank: UserBank
) {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Long? = null
    //should become a run strategy at some point
    var repeatable = false
}