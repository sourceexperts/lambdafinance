package com.sourceexperts.lambdafinance

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.scheduling.annotation.EnableScheduling


@SpringBootApplication
@EnableScheduling
class LambdaFinanceApplication

fun main(args: Array<String>) {
    runApplication<LambdaFinanceApplication>(*args)

}